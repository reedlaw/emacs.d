;;; init.el --- Load emacs config
;;; Commentary:
;;; Code:

(require 'org)
(org-babel-load-file "~/.emacs.d/reed.org")

(provide 'init)
;;; init.el ends here
